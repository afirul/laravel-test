<?php

use Illuminate\Database\Seeder;
use App\Entity\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$user = collect([
            ['name' => 'administrator', 'email' => 'admin@laravel.com', 'type' => 'a'],
            ['name' => 'user', 'email' => 'user@laravel.com', 'type' => 'u'],
    	])
    	->each(function ($user) {

            User::create([
                'name'      => $user['name'],
                'email'     => $user['email'],
                'password'  => bcrypt('password'),
                'type'  => $user['type'],
            ]);
        });
    }
}
