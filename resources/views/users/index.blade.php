@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3>User
                        <a href="{{ route('user.create') }}">
                            <button class="btn btn-success pull-right" style="float: right;">Create</button>
                        </a>
                    </h3>
                </div>

                <div class="card-body">
                    
                   @if(session('success-msg'))
                    <div class="alert alert-success" role="alert">
                        <p>{{ session('success-msg') }}</p>
                    </div>
                    @endif

                    @if(session('error-msg'))
                    <div class="alert alert-success" role="alert">
                        <p>{{ session('error-msg') }}</p>
                    </div>
                    @endif

                   <table class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <th>{{ $user->name }}</th>
                                    <th>{{ $user->email }}</th>
                                    <th>{{ $user->created_at->toDateTimeString() }}</th>
                                    <th>
                                        <a href="{{ route('user.show', [$user->id]) }}">
                                            <button class="btn btn-default btn-sm">Show</button>
                                        </a>
                                        <a href="{{ route('user.edit', [$user->id]) }}">
                                            <button class="btn btn-info btn-sm">Edit</button>
                                        </a>

                                        <form action="{{ route('user.destroy', $user->id) }}" method="POST">
                                            @method('DELETE')
                                            @csrf
                                            <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                                        </form>
                                    </th>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                     <div style="float:left; margin-right: 5px;">
                        <div class="pagination">
                            <h5 class="page-item"> Showing {{$users->total()}} - {{$users->total()}} of {{$users->total()}}</h5>
                        </div>
                    </div>
                    <div style="float:right; margin-right: 5px;">
                        {!! $users->appends(\Request::except('page'))->render("pagination::bootstrap-4") !!} 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
