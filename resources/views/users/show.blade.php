@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3>View User Details
                        <a href="{{ route('user.index') }}">
                            <button class="btn btn-success pull-right" style="float: right;">Back</button>
                        </a>
                    </h3>
                </div>

                <div class="card-body">
                    

                    <div class="form-group col-md-6">
                        <label>Name</label>
                        <input type="text" name="name" value="{{ $user->name }}" class="form-control" placeholder="e.g Afirul Afzareza" disabled="">
                    </div>

                    <div class="form-group col-md-6">
                        <label>Email</label>
                        <input type="email" name="email" value="{{ $user->email }}" class="form-control" placeholder="e.g afirul@gmail.com" disabled="">
                    </div>
   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
