@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3>Create New User
                        <a href="{{ route('user.index') }}">
                            <button class="btn btn-success pull-right" style="float: right;">Back</button>
                        </a>
                    </h3>
                </div>

                <div class="card-body">
                    
                    <form action="{{ route('user.store') }}" method="POST">
                        @csrf

                            @if(session('errors'))
                                <div class="alert alert-danger" role="alert">
                                    <p>Errors</p>
                                    @foreach($errors->all() as $error)
                                    &bull;{{ $error }}<br>
                                    @endforeach
                                </div>
                            @endif

                            @if(session('success-msg'))
                                <div class="alert alert-success" role="alert">
                                    <p>{{ session('success-msg') }}</p>
                                </div>
                            @endif


                        <div class="form-group col-md-6">
                            <label>Name</label>
                            <input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="e.g Afirul Afzareza">
                        </div>

                        <div class="form-group col-md-6">
                            <label>Email</label>
                            <input type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="e.g afirul@gmail.com">
                        </div>

                        <div class="form-group col-md-6">

                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </form>
   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
