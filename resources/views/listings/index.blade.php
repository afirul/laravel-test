@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3>Listing
                        <a href="{{ route('listing.create') }}">
                            <button class="btn btn-success pull-right" style="float: right;">Create</button>
                        </a>
                    </h3>
                </div>

                <div class="card-body">
                    
                   @if(session('success-msg'))
                    <div class="alert alert-success" role="alert">
                        <p>{{ session('success-msg') }}</p>
                    </div>
                    @endif

                    @if(session('error-msg'))
                    <div class="alert alert-success" role="alert">
                        <p>{{ session('error-msg') }}</p>
                    </div>
                    @endif

                   <table class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Address</th>
                                <th>Latitude</th>
                                <th>Longitude</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>

                            @forelse($listings as $listing)
                            <tr>
                                
                                <td>{{ $listing->list_name }}</td>
                                <td>{{ $listing->address }}</td>
                                <td>{{ $listing->latitude }}</td>
                                <td>{{ $listing->longitude }}</td>
                                <td>
                                    <a href="{{ route('listing.show', [$listing->id]) }}">
                                        <button class="btn btn-default btn-sm">Show</button>
                                    </a>
                                    <a href="{{ route('listing.edit', [$listing->id]) }}">
                                        <button class="btn btn-info btn-sm">Edit</button>
                                    </a>

                                    <form action="{{ route('listing.destroy', $listing->id) }}" method="POST">
                                        @method('DELETE')
                                        @csrf
                                        <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="5">No Record Found</td>
                            </tr>
                            @endforelse
                           
                        </tbody>
                    </table>

                    <div style="float:left; margin-right: 5px;">
                        <div class="pagination">
                            <h5 class="page-item"> Showing {{$listings->total()}} - {{$listings->total()}} of {{$listings->total()}}</h5>
                        </div>
                    </div>
                    <div style="float:right; margin-right: 5px;">
                        {!! $listings->appends(\Request::except('page'))->render("pagination::bootstrap-4") !!} 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
