@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3>View Listing
                        <a href="{{ route('listing.index') }}">
                            <button class="btn btn-success pull-right" style="float: right;">Back</button>
                        </a>
                    </h3>
                </div>

                <div class="card-body">
                    
                    <div class="form-group col-md-6">
                        <label>Name</label>
                        <input type="text" name="name" value="{{ $listing->list_name }}" class="form-control" placeholder="e.g Starbucks" disabled="">
                    </div>

                    <div class="form-group col-md-6">
                        <label>Address</label>
                        <textarea class="form-control" name="address" disabled="">{{ $listing->address }}</textarea>
                    </div>

                    <div class="form-group col-md-6">
                        <label>Latitude</label>
                        <input type="text" name="latitude" value="{{ $listing->latitude }}" class="form-control" placeholder="e.g 3.2952" disabled="">
                    </div>

                    <div class="form-group col-md-6">
                        <label>Longitude</label>
                        <input type="text" name="longitude" value="{{ $listing->longitude }}" class="form-control" placeholder="e.g 3.2952" disabled="">
                    </div>

                    <div class="form-group col-md-6">
                        <label>Submitter</label>
                        <input type="text" name="longitude" value="{{ $listing->user->name }}" class="form-control" placeholder="e.g 3.2952" disabled="">
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
