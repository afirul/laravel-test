<?php

namespace App\Http\Middleware;

use App\Supports\ApiSettings;
use Closure;

class VerifyToken
{
    use ApiSettings;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->header('Authorization')){
            return $this->failedResponse($request, 'token_not_provided');
        }

        return $next($request);

    }
}
