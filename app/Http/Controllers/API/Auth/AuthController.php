<?php

namespace App\Http\Controllers\API\Auth;

use App\Entity\User;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Supports\ApiSettings;


class AuthController extends Controller
{
    

    use ApiSettings;
    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     */
    public function login(Request $request) { 
    	
    	$data = $this->data;

    	$request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);


    	if(Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])){ 
    		
    		$user = Auth::user(); 
    		$token = $user->createToken('Personal Access Token')->accessToken;

    		$user->access_token = $token;

    		array_push($data, $user);
            $user->appData = $this->prepareAppData($request, $data);

            return new UserResource($user);

    	}

    	##FAILED LOGIN
		return $this->failedResponse($request, 'Login Failed. Please Check User and Password', 401);
	}


    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }


    /**
     * Get the authenticated User
     *      
     * @return [json] user object
     */
    public function user(Request $request)
    {
        $data = $this->data;
    	$user = $request->user();

        array_push($data, $user);
        $user->appData = $this->prepareAppData($request, $data);

        return new UserResource($user);
    }
}
