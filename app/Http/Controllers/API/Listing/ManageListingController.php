<?php

namespace App\Http\Controllers\API\Listing;

use Validator;
use App\Entity\Listing;
use App\Http\Controllers\Controller;
use App\Http\Resources\ListingCollection;
use App\Supports\ApiSettings;
use Illuminate\Http\Request;

class ManageListingController extends Controller
{

	use ApiSettings;

    public function index(Request $request) { 

    	$data = $this->data;

    	$rules = [
            'latitude'            => 'required',
            'longitude'           => 'required',
        ];

        $message = [];

        $validator = Validator::make($request->input(), $rules, $message);

        if ($validator->fails()) {
            return $this->failedResponse($request, $validator->errors()->first());
        }


    	$listings = Listing::distance($request->input('latitude'), $request->input('longitude'))->get();

		array_push($data, $listings);

		$appData = $this->prepareAppData($request, $data);

        return (new ListingCollection($listings))->additional(['AppData' => $appData]);
	}
}
